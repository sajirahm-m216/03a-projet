#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------
#           EPTM - Ecole professionnelle technique et des métiers
#
# Nom du fichier source              : sajirahm_exa3.py
#
# Auteur (Nom, Prénom)               : Sajidur Rahman
# Classe                             : EM-IN DEV 2A
# Module                             : M216
# Date de création                   : 25.03.2024
#
# Description succincte du programme :
#    Gestion des capteurs et actionneurs d'une centrale nucléaire.
#----------------------------------------------------------------------------

import random
import time

from grovepi import *
from grove_rgb_lcd import *

from paho.mqtt import client as mqtt_client


broker = 'mqtt-eptm.jcloud.ik-server.com'
port = 11521

visa = 'sajirahm'

topic_niveau = visa + "/niveau"
topic_canaux_refroidissement = visa + "/canaux_refroidissement"
topic_canaux_etat = visa + "/canaux_etat"
topic_evacuation = visa + "/evacuation"
max_qos = 2

max_analog_read = 1023
max_radioactivite = 2000 # mSV
min_radioactivite = 0 # mSV

# generate client ID with pub prefix randomly
client_id = f'python-mqtt-{random.randint(0, 100)}'
username = ''
password = ''

connected = False

niveau_radioactivite = -1
canaux_etat = "n"

pin_pot = 0
pin_button = 1
pin_buzzer = 3

pinMode(pin_pot, "INPUT")
pinMode(pin_button, "INPUT")
pinMode(pin_buzzer, "OUTPUT")

def connect_mqtt() -> mqtt_client:
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client

def subscribe(client: mqtt_client):
    def on_message(client, userdata, msg):
        if msg.topic == topic_niveau:
            value = int(msg.payload.decode())
            global niveau_radioactivite
            niveau_radioactivite = value
        elif msg.topic == topic_canaux_refroidissement :
            value = int(msg.payload.decode())
            digitalWrite(pin_buzzer, value)
            global canaux_etat
            canaux_etat = "Y" if value == 1 else "n"
        elif msg.topic == topic_evacuation:
            value = int(msg.payload.decode())
            digitalWrite(pin_buzzer, value)

    client.subscribe(topic_niveau, max_qos)
    client.subscribe(topic_canaux_refroidissement, max_qos)
    client.on_message = on_message
    
def readPin(pin, min, max, rnd = 0):
    return round(analogRead(pin) / max_analog_read * (max - min) + min, rnd)

def run():
    client = connect_mqtt()
    subscribe(client)
    client.loop_start()
    old_but = -1
    old_pot = -1
    old_niveau_radioactivite = None
    refresh = True
    while True:
        pot = int(readPin(pin_pot, min_radioactivite, max_radioactivite))
        but = analogRead(pin_button)

        # Activer les canaux et le buzzer quand le bouton est appuyé
        if(old_but != but):
            refresh = True
            old_but = but
            client.publish(topic_canaux_refroidissement, int(but != 0), retain=True)

        # Réglage de la radioactivité en fonction du potentiomètre
        if(old_pot != pot):
            refresh = True
            old_pot = pot
            client.publish(topic_niveau, pot, retain=True)
            if pot > 1250:
                client.publish(topic_evacuation, pot, retain=False)
            
        if(old_niveau_radioactivite != niveau_radioactivite):
            refresh = True
            old_niveau_radioactivite = niveau_radioactivite

        if(refresh):
            refresh = False
            dosage_str = "Y" if niveau_radioactivite > 1250 else "n"
            radioactivite_str = str(niveau_radioactivite).rjust(4)
            setText_norefresh(f"Radio: {radioactivite_str} mSV Dng: {dosage_str} Can: {canaux_etat}")

        time.sleep(0.5)

if __name__ == '__main__':
    run()
